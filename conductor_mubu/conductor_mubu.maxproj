{
	"name" : "conductor_mubu",
	"version" : 1,
	"creationdate" : 3629109677,
	"modificationdate" : 3797875804,
	"viewrect" : [ 25.0, 100.0, 300.0, 500.0 ],
	"autoorganize" : 0,
	"hideprojectwindow" : 0,
	"showdependencies" : 0,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"conductor_db_viewer.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"conductor.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"conductor_mubu_IDEA.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"conductor_mubu_JMF10.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"conductor_mubu_JMF11.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"conductor_mubu_JMF12.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"singleton" : 				{
					"bootpath" : "~/Projets/Geste/conductor_mubu/patchers",
					"projectrelativepath" : "./patchers"
				}

			}
,
			"conductor_mubu_JMF13.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"singleton" : 				{
					"bootpath" : "~/Projets/Geste/conductor_mubu/patchers",
					"projectrelativepath" : "./patchers"
				}

			}
,
			"conductor_mubu_JMF14.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"singleton" : 				{
					"bootpath" : "~/Projets/Geste/conductor_mubu/patchers",
					"projectrelativepath" : "./patchers"
				}

			}
,
			"conductor_mubu_JMF14-CONCERT.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"singleton" : 				{
					"bootpath" : "~/Projets/Geste/conductor_mubu/patchers",
					"projectrelativepath" : "./patchers"
				}

			}
,
			"conductor_mubu_JIM24.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"singleton" : 				{
					"bootpath" : "~/Projets/Geste/conductor_mubu/patchers",
					"projectrelativepath" : "./patchers"
				}

			}
,
			"stretchingTest1.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"singleton" : 				{
					"bootpath" : "~/Projets/Geste/conductor_mubu/patchers",
					"projectrelativepath" : "./patchers"
				}

			}
,
			"conductor_mubu_DEMO_JIM24.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1,
				"singleton" : 				{
					"bootpath" : "~/Projets/Geste/conductor_mubu/patchers",
					"projectrelativepath" : "./patchers"
				}

			}

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0,
	"amxdtype" : 1633771873,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : ".",
	"sortmode" : 0,
	"viewmode" : 0,
	"includepackages" : 0
}
