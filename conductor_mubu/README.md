# conductor_mubu
max patch to follow the chef d'orchestre with Ircam Gesture follower

## open *conductor_mubu.maxproj* in MAX 8

## dependencies
- mubu
- gestural sound toolkit

## howto generate a resumé from a long score
To simplify the model recording process, The goal is to record a score containing all the different battues from the whole score.
- import the score in bach format in the *bach* tab (paste replace works !)
- _putmarkers_ will put a numbered marker on every bar 
- makeBDL will create a "bar description list" dictionary (BDL.dict) in *bach* tab from *conductor_mubu_JMF10* max patch
- makeCDL will create a "context description list" (dict) where each bar is related to its neighbours
### option 1 : short resumé (one bar at a time)
- jump to *short_score* tab
- _generate resumé_ will generate the short score 
### option 2 : medium resumé (3 bar at a time, considering context)
- jump to *medium_score* tab
- _generate resumé_ will generate the short score
- exportmidi
- import the midifile into Ableton Live (or other) and generate a clicktrack soundfile

## howto generate a complete score from bar models  
### step 1 : record the bar models (in max)
- record the conductor (in the *follower* tab)
    - load the clickctrack audiofile
    - check the sensors
    - press *learn*
    - it is possible to follow the score in the *short_score* or *medium_score* subpatcher (select the *learning_scroll* mode)
- save 
	- the ddl dict
	- the markers
	- the recorded gesture (with "writedata 1" as .txt)

### step2 : generate the full score (in python)
cf conductor_python/mas.py to :
- import the models
- generate the full score
    - _make\_long\_seq.py_ shows example of how to create long score using the *conductor* python package
        - (see illustration : (_conductor_follower\_doc\_graph_)   )
    - export it to a csv text file to be import in mubu gesture follower
    - load the data (in csv form) with "readdata 1" directly in *follower* or 
    - in the *mubuconversion* tab :
        - load the data (in csv form) with "readdata 1"
        - load the markers
        - save in mubu format

### step 3 : following the full score (back to max)
- import (*data* menu in *follower* tab)

### step 4 : testing the follower
- press follow in *follower*
- load a score in *main* tab
- play
- goto to *bach* tab 
    - choose the *following scroll* mode to see the score synchronized with the conductor tempo and beats
    - or *sync* to play the bach score in sync with the gesture

## examples 
### 3 versions à suivre en parallele:
long.tutti1.mubu

### jmf project mai 2023 (almost everything)
- full version : 441 bars / 19'20"
- short (1) : 33 lesures
- medium 133 bars / 6'00"
    - clicktrack_medium_230504.aif
    - markers_medium_230504.txt
    
### simple demos for JIM 2024
#### tempo and beat follower
- open _conductor_mubu_demoJIM_
- choose a bar signature and tempo in the audio clicktrack menu
- record 4 bars
- follow and listen to the metronome (max transport)

##### creating a new clictrack
- use ableton live
- add markers on first beats in mubu
- save markers as .txt with message _writemarkers_

#### demo with video alignment
- Beethoven 2ndsymphony conducted byLeo Margue

## offline testing
gf has an offline mode 

## machine learning
